import { lazy } from "react";
import { createBrowserRouter } from "react-router-dom";

const Layout = lazy(() => import("components/Layout/Layout"));
const Posts = lazy(() => import("pages/Posts/Posts"));
const CreatePost = lazy(() => import("pages/CreatePost/CreatePost"));
const SignUp = lazy(() => import("pages/SignUp/SignUp"));
const SignIn = lazy(() => import("pages/SignIn/SignIn"));

export const router = createBrowserRouter([
  {
    path: "/signUp",
    element: <SignUp />,
  },
  {
    path: "/signIn",
    element: <SignIn />,
  },
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/posts",
        element: <Posts />,
      },
      {
        path: "/posts/create",
        element: <CreatePost />,
      },
      {
        path: "/posts/edit/:postId",
        element: <CreatePost />,
      },
    ],
  },
]);
