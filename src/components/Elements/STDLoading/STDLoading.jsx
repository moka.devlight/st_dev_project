import React from "react";
import { Spin } from "antd";

function STDLoading({ ...props }) {
  return <Spin {...props} />;
}

export default STDLoading;
