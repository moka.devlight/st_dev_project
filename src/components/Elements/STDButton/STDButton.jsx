import { Button } from "antd";

function STDButton({ type, children, ...props }) {
  return (
    <Button type={type} {...props}>
      {children}
    </Button>
  );
}

export default STDButton;
