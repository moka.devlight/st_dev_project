import React from "react";
import { Checkbox } from "antd";

function STDCheckBox({ title, ...props }) {
  return <Checkbox {...props}>{title}</Checkbox>;
}

export default STDCheckBox;
