import { Input } from "antd";

function STDInputPassword({ ...props }) {
  return <Input.Password {...props} size={"large"} />;
}

export default STDInputPassword;
