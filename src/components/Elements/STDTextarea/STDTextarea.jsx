import { Input } from "antd";

function STDTextarea({ maxLength, rows, ...props }) {
  const { TextArea } = Input;
  return <TextArea rows={rows} maxLength={maxLength} {...props} />;
}

export default STDTextarea;
