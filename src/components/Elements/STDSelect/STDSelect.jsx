import React from "react";
import { Select } from "antd";

function STDSelect({
  options,
  handleChange,
  defaultValue,
  placement,
  ...props
}) {
  return (
    <Select
      defaultValue={defaultValue}
      onChange={handleChange}
      options={options}
      placement={placement}
      size="large"
      style={{
        width: "100%",
      }}
      {...props}
    />
  );
}

export default STDSelect;
