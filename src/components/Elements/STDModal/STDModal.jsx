import { Modal } from "antd";

function STDModal({ children, ...props }) {
  return <Modal {...props}>{children}</Modal>;
}

export default STDModal;
