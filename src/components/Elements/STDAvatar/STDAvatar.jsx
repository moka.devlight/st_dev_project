import { Avatar } from "antd";
import React from "react";

function STDAvatar({ icon, size, ...props }) {
  return <Avatar size={size} icon={icon} {...props} />;
}

export default STDAvatar;
