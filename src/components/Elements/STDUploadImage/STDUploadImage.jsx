import React, { useEffect, useState } from "react";
import { Modal, Upload } from "antd";
import STDImage from "../STDImage";
import { BiPlus } from "react-icons/bi";

const STDUploadImage = ({
  setPic,
  defaultvaluePic,
  stateModal,
  maxCount = 1,
  ...props
}) => {
  const [infoImage, setInfoImage] = useState({
    fileList: [],
    previewImage: "",
    previewTitle: "",
    previewVisible: false,
  });

  useEffect(() => {
    if (defaultvaluePic) {
      setInfoImage({
        ...infoImage,
        fileList: defaultvaluePic
          ? [
              {
                uid: "1",
                name: "example.jpg",
                status: "done",
                url: defaultvaluePic,
              },
            ]
          : [],
      });
    }
    if (stateModal?.mode === "create") {
      setInfoImage({
        fileList: [],
        previewImage: "",
        previewTitle: "",
        previewVisible: false,
      });
    }
  }, [defaultvaluePic, stateModal]);

  const handleCancel = () =>
    setInfoImage({ ...infoImage, previewVisible: false });

  const handlePreview = (file) => {
    setInfoImage({
      ...infoImage,
      previewImage: file.url || file?.preview,
      previewTitle:
        file.name || file?.url.substring(file?.url.lastIndexOf("/") + 1),
      previewVisible: true,
    });
  };

  const uploadButton = (
    <div className="flex flex-col justify-center items-center">
      <BiPlus />
      <div
        style={{
          marginTop: 1,
        }}
      >
        Upload
      </div>
    </div>
  );

  const dummyRequest = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 0);
  };

  function onChange({ file, fileList }) {
    setInfoImage({ fileList });
    setPic({ file });
  }

  return (
    <>
      <Upload
        accept="/png, /jpeg"
        onPreview={handlePreview}
        fileList={infoImage.fileList}
        customRequest={dummyRequest}
        onChange={onChange}
        {...props}
      >
        {infoImage?.fileList?.length >= maxCount ? null : uploadButton}
      </Upload>
      <Modal
        open={infoImage.previewVisible}
        title={infoImage.previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <STDImage
          alt="UploadImage"
          style={{
            width: "100%",
          }}
          src={infoImage.previewImage}
        />
      </Modal>
    </>
  );
};
export default STDUploadImage;
