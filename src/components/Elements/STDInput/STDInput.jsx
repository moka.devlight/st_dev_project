import { Input } from "antd";

function STDInput({ ...props }) {
  return <Input {...props} size={"large"} />;
}

export default STDInput;
