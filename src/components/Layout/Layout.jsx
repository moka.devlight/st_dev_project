import React from "react";
import Header from "./Header/Header";
import { Outlet } from "react-router-dom";

function Layout({ children }) {
  return (
    <>
      <div>
        <Header />
      </div>
      <div>
        <Outlet />
        {children}
      </div>
    </>
  );
}

export default Layout;
