import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import useFetchData from "utils/useFetchData";

function useHeader() {
  const navigate = useNavigate();
  const [dataHeader, setDataHeader] = useState(false);

  ////////////////////handleCuurentHeader
  const handleCuurentHeader = () => {
    useFetchData
      .get(`/api/user/me/`)
      .then((e) => {
        setDataHeader(e.data);
      })
      .catch(() => {
        toast.error("Unsuccessful");
      });
  };

  ////////////////////log out
  const handleLogout = () => {
    const data = {
      refresh: localStorage.getItem("JWTR") ?? sessionStorage.getItem("JWTR"),
    };
    useFetchData
      .post(`/api/user/logout/`, data)
      .then(() => {
        navigate("/signIn");
        localStorage.clear();
        sessionStorage.clear();
        Cookies.remove("currentUser");
      })
      .catch(() => {
        toast.error("Unsuccessful");
      });
  };

  useEffect(() => {
    handleCuurentHeader();
  }, []);

  return { dataHeader, handleLogout };
}

export default useHeader;
