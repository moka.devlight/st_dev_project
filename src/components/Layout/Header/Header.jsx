import React from "react";
import STDAvatar from "components/Elements/STDAvatar";
import { BiUser } from "react-icons/bi";
import { Link } from "react-router-dom";
import useHeader from "./useHeader";

function Header() {
  const { dataHeader, handleLogout } = useHeader();

  return (
    <div className="w-full h-24 shadow-md bg-white">
      <div className="h-full flex justify-around items-center">
        <div>
          <Link
            to={"/posts"}
            className="text-[#28A4DA] text-lg font-bold no-underline"
          >
            Posts
          </Link>
        </div>
        <div className="flex justify-start items-center gap-4">
          <STDAvatar
            size={60}
            //src={<BiUser />}
            src={dataHeader?.image ?? <BiUser />}
            className={"bg-slate-300"}
          />
          <p className="text-base font-medium">{`${dataHeader?.first_name} ${dataHeader?.last_name}`}</p>
          <p
            className="text-[#0089C9] text-base font-medium cursor-pointer"
            onClick={() => handleLogout()}
          >
            Logout
          </p>
        </div>
      </div>
    </div>
  );
}

export default Header;
