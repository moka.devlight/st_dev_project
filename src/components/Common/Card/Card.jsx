import React, { useState } from "react";
import STDImage from "components/Elements/STDImage";
import { RiDeleteBinLine } from "react-icons/ri";
import { MdEdit } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import STDModal from "components/Elements/STDModal";
import DeleteForm from "./components/DeleteForm";

function Card({ id, Pic, title, description, category, onDelete }) {
  const navigate = useNavigate();
  const [deleteModal, setDeleteModal] = useState(false);

  function handleCloseModalAdd() {
    setDeleteModal(false);
  }
  return (
    <div className="grid grid-cols-3 w-full h-[207px] rounded-lg shadow-md">
      <div className="col-span-1 flex justify-center items-center rounded-lg">
        <STDImage
          src={Pic}
          alt={"pic"}
          className={"w-full h-[206px] rounded-lg"}
        />
      </div>
      <div className="col-span-2 px-2 flex flex-col justify-between py-3">
        <div className="flex justify-between items-center">
          <div>
            <p className="text-base font-bold text-[#444444] m-0">Name</p>
            <p className="text-base font-normal m-0 truncate" title={title}>
              {title}
            </p>
          </div>
          <div className="flex justify-end items-center gap-2">
            <MdEdit
              color="#28A4DA"
              size={20}
              className="cursor-pointer"
              onClick={() => navigate(`/posts/edit/${id}`)}
            />
            <RiDeleteBinLine
              color="#8A0000"
              size={20}
              className="cursor-pointer"
              onClick={() => setDeleteModal(true)}
            />
          </div>
        </div>
        <div>
          <p className="text-base font-bold text-[#444444] m-0">Description</p>
          <p className="text-base font-normal m-0 truncate" title={description}>
            {description}
          </p>
        </div>
        <div>
          <p className="text-base font-bold text-[#444444] m-0">Category</p>
          <p
            className="text-base font-normal m-0 truncate"
            title={category?.name}
          >
            {category?.name}
          </p>
        </div>
      </div>
      <STDModal
        open={deleteModal}
        width={"600px"}
        footer={null}
        onCancel={handleCloseModalAdd}
      >
        <DeleteForm
          setDeleteModal={setDeleteModal}
          onDelete={() => onDelete(id)}
        />
      </STDModal>
    </div>
  );
}

export default Card;
