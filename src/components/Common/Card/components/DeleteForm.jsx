import STDButton from "components/Elements/STDButton";
import React from "react";

function DeleteForm({ setDeleteModal, onDelete }) {
  return (
    <div className="text-center">
      <p className="text-2xl font-medium text-black">
        Are you sure you want to delete the post?
      </p>
      <div className="flex flex-col justify-center items-center gap-2 py-10">
        <STDButton
          type={"primary"}
          size={"large"}
          className={"w-[193px] text-base font-semibold bg-[#DA2828]"}
          onClick={onDelete}
        >
          Delete
        </STDButton>
        <STDButton
          type={"text"}
          size={"large"}
          className={"w-[193px] text-base font-semibold"}
          onClick={() => setDeleteModal(false)}
        >
          Cancel
        </STDButton>
      </div>
    </div>
  );
}

export default DeleteForm;
