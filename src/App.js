import STDLoading from "components/Elements/STDLoading";
import { Suspense, useEffect } from "react";
import { RouterProvider } from "react-router-dom";
import { router } from "routes/routesDefinition";

function App() {
  useEffect(() => {
    if (
      !localStorage.getItem("JWT") &&
      !sessionStorage.getItem("JWT") &&
      window.location.pathname !== "/signIn"
    ) {
      window.location.href = "/signIn";
    }
  }, []);

  return (
    <Suspense
      fallback={
        <STDLoading
          className={"flex justify-center items-center h-screen"}
          size={"large"}
        />
      }
    >
      <RouterProvider router={router} />
    </Suspense>
  );
}

export default App;
