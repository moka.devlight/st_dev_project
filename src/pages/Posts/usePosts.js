import { useState } from "react";
import toast from "react-hot-toast";
import useFetchData from "utils/useFetchData";

function usePosts() {
  const [loading, setLoading] = useState(false);
  const [pages, setPages] = useState({
    page: 1,
    pageSize: 5,
  });
  const [dataPosts, setDataPosts] = useState();

  ////////////////////getPosts
  const getPosts = () => {
    setLoading(true);
    useFetchData
      .get(`/api/post/crud/?limit=${pages.pageSize}&offset=${pages.page}`)
      .then((e) => {
        setDataPosts(e?.data);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        toast.error(e?.response?.data?.message ?? "Unsuccessful");
      });
  };

  ////////////////////DeletePost
  const deletePost = (id) => {
    setLoading(true);
    useFetchData
      .delete(`/api/post/crud/${id}/`)
      .then(() => {
        setLoading(false);
        toast.success("Successful");
        getPosts();
      })
      .catch((e) => {
        setLoading(false);
        toast.error(e?.response?.data?.message ?? "Unsuccessful");
      });
  };

  return { pages, setPages, loading, getPosts, dataPosts, deletePost };
}

export default usePosts;
