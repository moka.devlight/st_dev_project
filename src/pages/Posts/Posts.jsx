import React, { useEffect } from "react";
import { GoPlus } from "react-icons/go";
import STDButton from "components/Elements/STDButton";
import Card from "components/Common/Card";
import STDPagination from "components/Elements/STDPagination";
import { useNavigate } from "react-router-dom";
import usePosts from "./usePosts";
import STDLoading from "components/Elements/STDLoading";

function Posts() {
  const navigate = useNavigate();
  const { pages, setPages, loading, getPosts, dataPosts, deletePost } =
    usePosts();

  useEffect(() => {
    getPosts();
  }, [pages]);

  const handlePageChange = (page, pageSize) => {
    setPages({ pageSize: pageSize, page: page });
  };

  return (
    <div className="px-[335px] pt-16 pb-20">
      {loading ? (
        <div className="flex justify-center items-center">
          <STDLoading />
        </div>
      ) : (
        <>
          <div className="flex justify-between items-center">
            <div className="text-4xl font-semibold text-[#444444]">
              <p>All Posts</p>
            </div>
            <div className="flex justify-center items-center">
              <STDButton
                type={"primary"}
                size={"large"}
                htmlType="submit"
                className={
                  "w-[193px] text-base font-semibold bg-[#28A4DA] flex justify-center items-center"
                }
                icon={<GoPlus size={20} />}
                onClick={() => navigate("/posts/create")}
              >
                New Post
              </STDButton>
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            {dataPosts?.results?.map((item) => (
              <Card
                key={item?.id}
                id={item?.id}
                Pic={item?.image}
                category={item?.category}
                description={item?.description}
                title={item?.title}
                onDelete={deletePost}
              />
            ))}
          </div>
          <div className="flex justify-center items-center mt-9">
            <STDPagination
              defaultCurrent={1}
              current={pages.page}
              pageSize={pages.pageSize}
              total={dataPosts?.count}
              onChange={handlePageChange}
            />
          </div>
        </>
      )}
    </div>
  );
}

export default Posts;
