import STDButton from "components/Elements/STDButton";
import STDInput from "components/Elements/STDInput";
import STDSelect from "components/Elements/STDSelect";
import STDTextarea from "components/Elements/STDTextarea";
import STDUploadImage from "components/Elements/STDUploadImage";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import useCreatePost from "./useCreatePost";
import { useParams } from "react-router-dom";
import STDLoading from "components/Elements/STDLoading";

function CreatePost() {
  const { postId } = useParams();
  const [pic, setPic] = useState();
  const {
    loading,
    loadingPage,
    createPost,
    dataCategory,
    EditPost,
    initialDataPost,
  } = useCreatePost();
  const {
    handleSubmit,
    formState: { errors },
    control,
    setValue,
  } = useForm();

  useEffect(() => {
    setValue("title", initialDataPost?.title);
    setValue("description", initialDataPost?.description);
    setValue("category", {
      value: initialDataPost?.category?.id,
      label: initialDataPost?.category?.name,
    });
  }, [initialDataPost]);

  const dataCategoryOption = dataCategory?.map((item) => {
    return { value: item.id, label: item.name };
  });

  function onSubmit(data) {
    if (postId) {
      EditPost(data, pic);
    } else {
      createPost(data, pic);
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {loadingPage ? (
        <div className="flex justify-center items-center mt-4">
          <STDLoading />
        </div>
      ) : (
        <div className="w-1/2 px-96 mx-auto text-center">
          <div>
            <p className="text-4xl font-semibold text-[#444444]">
              {postId ? "Edit post" : "New post"}
            </p>
          </div>
          {/* ///////////Upload Image */}
          <div className="my-10 w-1/2 m-auto">
            <STDUploadImage
              listType="picture-card"
              setPic={setPic}
              defaultvaluePic={initialDataPost?.image}
            />
          </div>
          {/* ////////////Name */}
          <div className={"w-1/2 m-auto"}>
            <Controller
              name="title"
              control={control}
              rules={{ required: true }}
              render={({ field }) => (
                <STDInput
                  placeholder="Name"
                  size={"large"}
                  type="string"
                  {...field}
                />
              )}
            />
            {errors.title?.type === "required" && (
              <p
                className="text-[#8A0000] text-left m-0 text-xs font-medium"
                role="alert"
              >
                This filed is required
              </p>
            )}
          </div>
          {/* //////////TextArea */}
          <div className="my-4 w-1/2 m-auto">
            <Controller
              name="description"
              control={control}
              rules={{ required: true }}
              render={({ field }) => (
                <STDTextarea
                  rows={5}
                  maxLength={1200}
                  placeholder="Description"
                  {...field}
                />
              )}
            />
            {errors.description?.type === "required" && (
              <p
                className="text-[#8A0000] text-left m-0 text-xs font-medium"
                role="alert"
              >
                This filed is required
              </p>
            )}
          </div>
          {/* //////////Select */}
          <div className={"w-1/2 m-auto"}>
            <Controller
              name="category"
              control={control}
              rules={{ required: true }}
              render={({ field }) => (
                <STDSelect
                  options={dataCategoryOption}
                  placement={"bottomRight"}
                  className={"text-left"}
                  placeholder="Category"
                  {...field}
                />
              )}
            />
            {errors.category?.type === "required" && (
              <p
                className="text-[#8A0000] text-left m-0 text-xs font-medium"
                role="alert"
              >
                This filed is required
              </p>
            )}
          </div>
          {/* ////////////////btn */}
          <div className="my-10">
            <STDButton
              type={"primary"}
              size={"large"}
              htmlType="submit"
              loading={loading}
              className={"w-[176px] text-base font-semibold bg-[#28A4DA]"}
            >
              {postId ? "Edit" : "Create"}
            </STDButton>
          </div>
        </div>
      )}
    </form>
  );
}

export default CreatePost;
