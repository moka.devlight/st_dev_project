import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { useParams } from "react-router-dom";
import useFetchData from "utils/useFetchData";

function useCreatePost() {
  const { postId } = useParams();
  const [loading, setLoading] = useState(false);
  const [loadingPage, setLoadingPage] = useState(false);
  const [dataCategory, setDataCategory] = useState();
  const [initialDataPost, setinitialDataPost] = useState();

  ////////////////////createPost
  const createPost = (data, pic) => {
    setLoading(true);
    const initialDataPost = {
      title: data?.title,
      description: data?.description,
      category: data?.category,
      image: pic?.file?.originFileObj,
    };
    useFetchData
      .post(`/api/post/crud/`, initialDataPost)
      .then(() => {
        setLoading(false);
        toast.success("post create successful");
      })
      .catch((e) => {
        setLoading(false);
        toast.error(e?.response?.data?.message ?? "post create Unsuccessful");
      });
  };

  ////////////////////editPost
  const EditPost = (data, picUrl) => {
    setLoading(true);
    const initialDataPost = {
      title: data?.title,
      description: data?.description,
      category: data?.category?.value ? data?.category?.value : data?.category,
      image: picUrl?.file ? picUrl?.file?.originFileObj : "",
    };
    useFetchData
      .put(`/api/post/crud/${postId}/`, initialDataPost)
      .then(() => {
        setLoading(false);
        toast.success("post edit successful");
      })
      .catch((e) => {
        setLoading(false);
        toast.error("post edit Unsuccessful");
      });
  };

  ////////////////////getPost by id
  const getPostId = () => {
    setLoadingPage(true);
    useFetchData
      .get(`/api/post/crud/${postId}/`)
      .then((e) => {
        setinitialDataPost(e.data);
        setLoadingPage(false);
      })
      .catch((e) => {
        setLoadingPage(false);
        toast.error(e?.response?.data?.message ?? "get Post Unsuccessful");
      });
  };

  ////////////////////getCategory
  const getCategory = () => {
    setLoading(true);
    useFetchData
      .get(`/api/category/`)
      .then((e) => {
        setDataCategory(e.data);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        toast.error(e?.response?.data?.message ?? "get Category Unsuccessful");
      });
  };

  useEffect(() => {
    getCategory();
    if (postId) {
      getPostId();
    }
  }, []);

  return {
    loading,
    loadingPage,
    createPost,
    dataCategory,
    EditPost,
    initialDataPost,
  };
}

export default useCreatePost;
