import Cookies from "js-cookie";
import { useState } from "react";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import useFetchData from "utils/useFetchData";

function useSignIn() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  ////////////////////SignIn
  const handleSignIn = (payload, rememberMe) => {
    setLoading(true);
    const data = {
      email: payload?.email,
      password: payload?.password,
    };
    useFetchData
      .post(`/api/user/sign-in/`, data)
      .then((e) => {
        setLoading(false);
        toast.success("successful");
        if (rememberMe) {
          localStorage.setItem("JWT", e.data.token.access);
          localStorage.setItem("JWTR", e.data.token.refresh);
          Cookies.set("currentUser", "currentUser");
        } else {
          sessionStorage.setItem("JWT", e.data.token.access);
          sessionStorage.setItem("JWTR", e.data.token.refresh);
          Cookies.set("currentUser", "currentUser");
        }
        navigate("/posts");
      })
      .catch((e) => {
        setLoading(false);
        toast.error(e?.response?.data?.message ?? "Unsuccessful");
      });
  };

  return { loading, handleSignIn };
}

export default useSignIn;
