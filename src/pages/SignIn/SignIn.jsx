import React, { useState } from "react";
import STDButton from "components/Elements/STDButton";
import STDInput from "components/Elements/STDInput";
import { Controller, useForm } from "react-hook-form";
import useSignIn from "./useSignIn";
import { Link } from "react-router-dom";
import STDCheckBox from "components/Elements/STDCheckBox";
import STDInputPassword from "components/Elements/STDInputPassword";

function SignIn() {
  const [rememberMe, setRememberMe] = useState(false);
  const { loading, handleSignIn } = useSignIn();
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();

  const handleRememberMe = () => {
    setRememberMe(!rememberMe);
  };

  function onSubmit(data) {
    handleSignIn(data, rememberMe);
    if (rememberMe) {
      localStorage.setItem("rememberMe", "rememberMe");
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="h-screen bg-[#009DDF] flex items-center justify-center">
        <div className="bg-white w-[480px] h-[404px] rounded-xl">
          <div className="w-full h-full flex justify-center items-center">
            <div className="content md:w-3/4 text-center">
              <div className="text-center text-[#28A4DA] font-semibold text-3xl my-7">
                {"Sign In"}
              </div>
              {/* /////Form */}
              <div className="grid grid-cols-1 gap-4">
                <div>
                  <Controller
                    name="email"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <STDInput
                        placeholder="Email"
                        size={"large"}
                        type="email"
                        {...field}
                      />
                    )}
                  />
                  {errors.email?.type === "required" && (
                    <p
                      className="text-[#8A0000] text-left m-0 text-xs font-medium"
                      role="alert"
                    >
                      This filed is required
                    </p>
                  )}
                </div>
                <div>
                  <Controller
                    name="password"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <STDInputPassword
                        placeholder="Password"
                        size={"large"}
                        {...field}
                      />
                    )}
                  />
                  {errors.Password?.type === "required" && (
                    <p
                      className="text-[#8A0000] text-left m-0 text-xs font-medium"
                      role="alert"
                    >
                      This filed is required
                    </p>
                  )}
                </div>
                <div className="text-left">
                  <STDCheckBox
                    title={"Remember me"}
                    checked={rememberMe}
                    onChange={handleRememberMe}
                  />
                </div>
              </div>
              {/* /////submit btn */}
              <div className="w-full flex justify-center items-center mt-16">
                <STDButton
                  type={"primary"}
                  size={"large"}
                  htmlType="submit"
                  loading={loading}
                  className={"w-[176px] text-base font-semibold bg-[#28A4DA]"}
                >
                  Sign In
                </STDButton>
              </div>
              <div className="flex justify-center items-center gap-1">
                <p>Need an account?</p>
                <Link to={"/signUp"} className="text-[#0089C9] no-underline">
                  Sign Up
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

export default SignIn;
