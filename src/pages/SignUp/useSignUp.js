import { useState } from "react";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import useFetchData from "utils/useFetchData";

function useSignUp() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  ////////////////////SignUp
  const handleSignUp = (data, pic) => {
    setLoading(true);
    const dataForm = {
      ...data,
      image: pic?.file?.originFileObj,
    };
    delete dataForm?.ConfirmPassword;

    useFetchData
      .post(`/api/user/sign-up/`, dataForm)
      .then((e) => {
        setLoading(false);
        toast.success("successful");
        navigate("/signIn");
      })
      .catch((e) => {
        setLoading(false);
        toast.error(e?.response?.data?.message ?? "Unsuccessful");
      });
  };

  return { loading, handleSignUp };
}

export default useSignUp;
