import React, { useState } from "react";
import STDButton from "components/Elements/STDButton";
import STDInput from "components/Elements/STDInput";
import { Controller, useForm } from "react-hook-form";
import useSignUp from "./useSignUp";
import STDUploadImage from "components/Elements/STDUploadImage/STDUploadImage";
import { Link } from "react-router-dom";
import toast from "react-hot-toast";
import STDInputPassword from "components/Elements/STDInputPassword";

function SignUp() {
  const [pic, setPic] = useState();
  const { loading, handleSignUp } = useSignUp();
  const {
    handleSubmit,
    formState: { errors },
    control,
    watch,
  } = useForm();

  function containsUppercase(value) {
    return /[A-Z]/.test(value);
  }

  function onSubmit(data) {
    if (watch("ConfirmPassword") === watch("password")) {
      handleSignUp(data, pic);
    } else {
      toast.error("Passwords do not match!");
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="h-screen bg-[#009DDF] flex items-center justify-center">
        <div className="bg-white w-[781px] h-[622px] rounded-xl">
          <div className="w-full h-full flex justify-center items-center">
            <div className="content md:w-3/4 text-center">
              <div className="text-center text-[#28A4DA] font-semibold text-3xl">
                {"Sign Up"}
              </div>
              <div className="my-10">
                <STDUploadImage listType="picture-circle" setPic={setPic} />
              </div>
              <div className="grid grid-cols-2 gap-4">
                <div>
                  <Controller
                    name="first_name"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <STDInput
                        placeholder="First Name"
                        size={"large"}
                        type="string"
                        {...field}
                      />
                    )}
                  />
                  {errors.first_name?.type === "required" && (
                    <p
                      className="text-[#8A0000] text-left m-0 text-xs font-medium"
                      role="alert"
                    >
                      This filed is required
                    </p>
                  )}
                </div>
                <div>
                  <Controller
                    name="last_name"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <STDInput
                        placeholder="Last Name"
                        size={"large"}
                        type="string"
                        {...field}
                      />
                    )}
                  />
                  {errors.last_name?.type === "required" && (
                    <p
                      className="text-[#8A0000] text-left m-0 text-xs font-medium"
                      role="alert"
                    >
                      This filed is required
                    </p>
                  )}
                </div>
                <div>
                  <Controller
                    name="email"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <STDInput
                        placeholder="Email"
                        size={"large"}
                        type="email"
                        {...field}
                      />
                    )}
                  />
                  {errors.email?.type === "required" && (
                    <p
                      className="text-[#8A0000] text-left m-0 text-xs font-medium"
                      role="alert"
                    >
                      This filed is required
                    </p>
                  )}
                </div>
                <div></div>
                <div>
                  <Controller
                    name="password"
                    control={control}
                    rules={{
                      required: "This filed is required",
                      minLength: {
                        value: 6,
                        message: "Password must be at least 6 characters",
                      },
                      validate: {
                        uppercase: (value) =>
                          containsUppercase(value) ||
                          "Password must contain at least one uppercase letter",
                      },
                    }}
                    render={({ field }) => (
                      <STDInputPassword
                        placeholder="Password"
                        size={"large"}
                        {...field}
                      />
                    )}
                  />
                  {errors.password && (
                    <p className="text-[#8A0000] text-left m-0 text-xs font-medium">
                      {errors.password.message}
                    </p>
                  )}
                </div>
                <div>
                  <Controller
                    name="ConfirmPassword"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <STDInputPassword
                        placeholder="Confirm Password"
                        size={"large"}
                        {...field}
                      />
                    )}
                  />
                  {errors.ConfirmPassword?.type === "required" && (
                    <p
                      className="text-[#8A0000] text-left m-0 text-xs font-medium"
                      role="alert"
                    >
                      This filed is required
                    </p>
                  )}
                </div>
              </div>
              <div className="w-full flex justify-center items-center mt-16">
                <STDButton
                  type={"primary"}
                  size={"large"}
                  htmlType="submit"
                  loading={loading}
                  className={"w-[176px] text-base font-semibold bg-[#28A4DA]"}
                >
                  Sign Up
                </STDButton>
              </div>
              <div className="flex justify-center items-center gap-1">
                <p>Already have an account?</p>
                <Link to={"/signIn"} className="text-[#0089C9] no-underline">
                  Sign In
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

export default SignUp;
